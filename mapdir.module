<?php
/**
 * mapdir.module - map functions over directory hierarchies.
 *
 * This is a very general framework for developers who need to work
 * with file hierarchies.
 *
 * @author Djun Kim <http://puregin.org/>
 * @copyright Copyright (c) 2008 Djun Kim. All rights reserved.
 * @license GPL <http://creativecommons.org/licenses/GPL/2.0/>
 * @package mapdir.module
 */


//////////////////////////////////////////////////////////////////////////////
// Core API hooks

/**
 * Implementation of hook_help().
 */
function mapdir_help($path) {
  switch ($path) {
    case 'admin/help#mapdir':
      return '<p>' . t('<a href="@mapdir" target="_blank">Mapdir</a> provides a  mechanism to traverse file system hierarchies, applying functions to each file.', array('@mapdir' => 'http://drupal.org/project/mapdir')) . '</p>';
  }
}

/**
 * Implementation of hook_perm().
 */
function mapdir_perm() {
  return array(
    'apply mapdir',
  );
}

/**
 * Implementation of hook_cron().
 */
function mapdir_cron() {
}

/**
 * Maps given function(s) over a given directory hierarchy.
 *
 * @param dir - A path to the directory to iterate over.
 *

 * @param args - An array of parameters which specify the 
 * order, and apply functions, and potentially other arguments.
 *
 * The order function is specified by $args['order_function]. It
 * should be a function which takes an array of files as a parameter
 * and returns an array of files, giving the order in which the files
 * are to be processed.
 *
 * The apply_function is an array of maps specified as key-value pairs
 * '<selector_function>' => '<apply_function>'. 
 * The map function is applied if the selector returns true for the file.
 * 
 * The functions are tested and applied in the order in which the
 * foreach() iterator returns them.
 *
 * If no default order function is specified, the inorder function provided
 * by this module will be used.
 *
 * If no apply function is specified, a utility function to record the
 * visit to the file in the watchdog table is used by default.
 * 
 */
function mapdir_iterate($dir, $args = array()) {
  //$argstr = print_r($args, TRUE);
  //watchdog('mapdir', "enter mapdir-iterate, $dir, $argstr", array(), WATCHDOG_DEBUG);
  if (!user_access('apply mapdir')) { return; }
  $order_function = isset($args['order_function']) ? 
    $args['order_function'] : 
    'mapdir_order_inorder';
  $apply_function = isset($args['apply_function']) ? 
    $args['apply_function'] : 
    array();
  if (!is_dir($dir)) {
    watchdog('error', "Mapdir: Directory $dir can't be found."); 
    return false;
  }
  if (count($apply_function) == 0) {
    // set up default behaviour: just print every filename.
    $apply_function = array(
      'mapdir_selector_nonempty' =>  'mapdir_apply_print_filename',
      'mapdir_selector_isdir' => 'mapdir_iterate',
      );
  }

  // Get the files in the given directory, ordered by the given order
  if (!function_exists($order_function)) {
    watchdog('mapdir', "specified order function $order_function does not exist", array(), WATCHDOG_ERROR);
    return false;
  }
  $files = $order_function(glob("$dir/*"));

  // Iterate over the files, applying each given function (selectively)
  foreach ($files as $file) {
    foreach ($apply_function as $selector => $apply) {
      if (function_exists($selector)) {
	if (function_exists($apply)) {
	  if ($selector($file)) {
	    $newargs = array (
			      'order_function' => $order_function,
			      'apply_function' => $apply_function,
			      );
	    $args = $newargs + $args;
	    $apply($file, &$args);
	    if (!isset($args)) {
	      $args = array();
	    }
	  }
	}
	else {
	  watchdog('mapdir', "specified apply function ($apply) does not exist", array(), WATCHDOG_ERROR);
	}
      }
      else {
	watchdog('mapdir', "specified selector function ($selector) does not exist", array(), WATCHDOG_ERROR);
      }
    }
  }
  // watchdog('mapdir', "leave mapdir-iterate, $dir, $argstr", array(), WATCHDOG_DEBUG);
}

/** 
 * Utility selector function; returns true if filename preg_matches
 * one of the given patterns.
 */
function mapdir_selector_file_preg_match($file, $patterns) {
  $file_parts = pathinfo($file);
  $filename = isset($file_parts['basename']) ? $file_parts['basename']: '';
  foreach ($patterns as $pattern) {
    preg_match($pattern, $filename, $matches);
    if (count($matches)) {
      return TRUE;
    }
  }
  return FALSE;
}

/** 
 * Utility selector function; returns true if filename has
 * one of the given extensions.
 */
function mapdir_selector_file_has_extn($file, $extns) {
  $file_parts = pathinfo($file);
  $ext = '.' . strtolower($file_parts['extension']);
  // the extensions returns by pathinfo do not include the '.' separator
  if ($ext == '.') {
    return FALSE;
  }
  else {
    if (in_array($ext, $extns)) {
      return TRUE;
    }
  }
  return FALSE;
}

/** 
 * Utility selector function; returns true if filename is not empty
 */
function mapdir_selector_nonempty($file) {
  if (strlen($file) > 0) {
    return TRUE;
  }
}

/** 
 * Utility selector function; returns true if file is a directory
 */
function mapdir_selector_isdir($file) {
  if (is_dir($file)) {
    return TRUE;
  }
}

/** 
 * Utility application function; prints name of the file 
 */
function mapdir_apply_print_filename($file) {
  watchdog('status', "mapdir: processing:$file");
  return TRUE;
}

/** 
 * Utility order function; returns files and directories in 
 * arranged in preorder sequence (directories first, alphabetically,
 * followed by non-directory files, also ordered alphabetically).
 * 
 * @param files - array of files to be sorted
 * @returns - array of files in 'preorder' sequence
 */
function mapdir_order_preorder($files) {
  if (count($files) == 0) {
    return array();
  }
  $dirs = array();
  $non_dirs = array();
  foreach ($files as $file) {
    if (is_dir($file)) {
      $dirs[] = $file;
    }
    else {
      $non_dirs[] = $file;
    }
  }
  asort($dirs);
  asort($non_dirs);
  $orderedfiles = $dirs;
  array_splice($orderedfiles, count($orderedfiles), 0, $non_dirs);
  return $orderedfiles;
}

/** 
 * Utility order function; returns files and directories in 
 * arranged in in-order sequence (files and directories intermixed, 
 * sorted by filename)
 * 
 * @param files - the list of files to be sorted
 * @returns - list of files in 'in-order' sequence
 */
function mapdir_order_inorder($files) {
  if (count($files) == 0) {
    return array();
  }
  asort($files);
  return $files;
}

/** 
 * Utility order function; returns files and directories arranged
 * in post-order sequence (non-directories first, alphabetically,
 * followed by directory files, also ordered alphabetically).
 * 
 * @param files - array of files to be sorted
 * @returns - array of files in 'post-order' sequence
 */
function mapdir_order_postorder($files) {
  if (count($files) == 0) {
    return array();
  }
  $dirs = array();
  $non_dirs = array();
  foreach ($files as $file) {
    if (is_dir($file)) {
      $dirs[] = $file;
    }
    else {
      $non_dirs[] = $file;
    }
  }
  asort($dirs);
  asort($non_dirs);
  $orderedfiles = $non_dirs;
  array_splice($orderedfiles, count($orderedfiles), 0, $dirs);
  return $orderedfiles;  
}


// tests
function _mapdir_test_iterate($dir) {
  mapdir_iterate($dir, 
		 array('order_function' => 'mapdir_order_postorder',)
		 );
}


